﻿using System;
using FirstApp.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using FirstApp.Services;

namespace FirstApp.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmployeesController : ControllerBase
    {
        private readonly IEmployeeCollectionService _employeeCollectionService;

        public EmployeesController(IEmployeeCollectionService employeeCollectionService)
        {
            _employeeCollectionService = employeeCollectionService;
        }

        /// <summary>
        /// Returns an employee by id.
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}", Name = "GetEmployeeById")]
        public async Task<IActionResult> GetEmployeeById(Guid id)
        {
            var employee = await _employeeCollectionService.Get(id);

            if (employee == null)
            {
                return NotFound();
            }

            return Ok(employee);
        }

        /// <summary>
        /// Returns a list of employees.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetEmployees()
        {
            return Ok(await _employeeCollectionService.GetAll());
        }

        /// <summary>
        /// Creates an employee.
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateEmployee([FromBody] Employee employee)
        {
            if (employee == null)
            {
                return BadRequest("Employee can not be null");
            }

            if (await _employeeCollectionService.Create(employee))
            {
                return CreatedAtRoute("GetEmployeeById", new {id = employee.Id}, employee);
            }

            return NoContent();
        }

        /// <summary>
        /// Update an employee.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateEmployee(Guid id, [FromBody] Employee employee)
        {
            var isUpdated = await _employeeCollectionService.Update(id, employee);
            if (!isUpdated)
                return NotFound();

            return Ok();
        }

        /// <summary>
        /// Delete an employee.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteEmployee(Guid id)
        {
            var deleted = await _employeeCollectionService.Delete(id);
            if (!deleted)
                return NotFound();

            return Ok();
        }
    }
}