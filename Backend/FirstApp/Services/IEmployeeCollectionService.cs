﻿using FirstApp.Models;

namespace FirstApp.Services
{
    public interface IEmployeeCollectionService: ICollectionService<Employee>
    {
        
    }
}
