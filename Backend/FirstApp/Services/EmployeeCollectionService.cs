﻿using FirstApp.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FirstApp.Settings;
using MongoDB.Driver;

namespace FirstApp.Services
{
    public class EmployeeCollectionService : IEmployeeCollectionService
    {
        private readonly IMongoCollection<Employee> _employees;

        public EmployeeCollectionService(IMongoDBSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _employees = database.GetCollection<Employee>(settings.EmployeeCollectionName);
        }

        public EmployeeCollectionService()
        {
        }

        public async Task<bool> Create(Employee employee)
        {
            await _employees.InsertOneAsync(employee);
            return true;
        }

        public async Task<Employee> Get(Guid id)
        {
            return (await _employees.FindAsync(employee => employee.Id == id)).FirstOrDefault();
        }

        public async Task<List<Employee>> GetAll()
        {
            return (await _employees.FindAsync(employee => true)).ToList();
        }

        public async Task<bool> Update(Guid id, Employee employee)
        {
            employee.Id = id;
            var result = await _employees.ReplaceOneAsync(e => e.Id == id, employee);
            return result.IsAcknowledged || result.ModifiedCount != 0;
        }

        public async Task<bool> Delete(Guid id)
        {
            var result = await _employees.DeleteOneAsync(employee => employee.Id == id);
            return result.IsAcknowledged || result.DeletedCount != 0;
        }
    }
}