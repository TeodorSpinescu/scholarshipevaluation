import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Employee } from '../_models/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeServiceService {

  
  employees: Employee[] = []

  readonly baseUrl= "https://localhost:5001";

  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  constructor(
    private httpClient: HttpClient
  ) { }

  getEmployees(): Observable<Employee[]> {
    return this.httpClient.get<Employee[]>(this.baseUrl+`/employees`, this.httpOptions)
  }

  delete(id: string): any {
    return this.httpClient.delete(this.baseUrl + "/employees/" + id);
  }

  addEmployee(employeeNew: Employee): any{
    return this.httpClient.post(this.baseUrl+"/employees", employeeNew, this.httpOptions).subscribe();
  }

  updateEmployee(id: string): any {
    return this.httpClient.put(this.baseUrl + "/employees" + id, id).subscribe()
  }

}
