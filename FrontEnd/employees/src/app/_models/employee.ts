export interface Employee {
    id?: string;
    firstname: string;
    lastname: string;
    address: string;
    department: string;
    position: string;
    creationDate: string;
}