import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddEmployeeComponent } from './app-employee/add-employee/add-employee.component';
import { PanelComponent } from './app-employee/panel/panel.component';

const appRoutes:Routes=[
  { path: "", component: PanelComponent, pathMatch: 'full' },
  { path: "addemployee", component: AddEmployeeComponent },
  { path: '**', redirectTo: ''}
]

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
