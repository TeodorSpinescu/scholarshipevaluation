import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppEmployeeModule } from './app-employee/app-employee.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { EmployeeServiceService } from './_services/employee-service.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AppEmployeeModule,
    NoopAnimationsModule,
    HttpClientModule
  ],
  providers: [
    EmployeeServiceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
