import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/_models/employee';
import { EmployeeServiceService } from 'src/app/_services/employee-service.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {

  employees: Employee[] = []

  constructor(
    private employeeService: EmployeeServiceService
  ) { }

  ngOnInit(): void {
    // this.employeeService.getEmployees().subscribe(result => {
    //   this.employees = result;
    // });
    this.employeeService.getEmployees().subscribe(result => {
      this.employees = result
    });
  }

  deleteEmployee(id: string): void {
    this.employeeService.delete(id).subscribe();
  }

}
