import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelComponent } from './panel/panel.component';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatGridListModule} from '@angular/material/grid-list';
import { EmployeeComponent } from './employee/employee.component';
import { MatCardModule } from "@angular/material/card";
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { MatInputModule} from '@angular/material/input';
import { MatFormFieldModule} from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { MatOptionModule } from '@angular/material/core';



@NgModule({
  declarations: [
    PanelComponent,
    EmployeeComponent,
    AddEmployeeComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatGridListModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,
    FormsModule,
    MatOptionModule
  ],
  exports: [
    PanelComponent,
    EmployeeComponent,
    AddEmployeeComponent
  ]
})
export class AppEmployeeModule { }
