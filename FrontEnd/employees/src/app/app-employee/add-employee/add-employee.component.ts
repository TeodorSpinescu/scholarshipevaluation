import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from 'src/app/_models/employee';
import { EmployeeServiceService } from 'src/app/_services/employee-service.service';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.scss']
})
export class AddEmployeeComponent implements OnInit {

  firstname: string
  lastname: string
  address: string 
  department: string
  position: string
  creationDate: string

  constructor(
    private employeeService: EmployeeServiceService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  addEmployee(): void {
    let employeeNew = {
      firstname: this.firstname,
      lastname: this.lastname,
      address: this.address,
      department: this.department,
      position: this.position,
      creationDate: this.creationDate
    }
      this.employeeService.addEmployee(employeeNew)
      this.router.navigateByUrl('')
  }

}
