import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employee } from 'src/app/_models/employee';
import { EmployeeServiceService } from 'src/app/_services/employee-service.service';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss']
})
export class PanelComponent implements OnInit {

  constructor(
    private employeeService: EmployeeServiceService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  addEmployee(): void{
    this.router.navigateByUrl('/addemployee');
  }

}
